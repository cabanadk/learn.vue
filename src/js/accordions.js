(function($) {

    if (!$('.accordion').length) {
        return;
    }

   $('.toggle').click(function(e) {
      e.preventDefault();
    
      var $this = $(this);
    
      if ($this.next().hasClass('show')) {
          $this.next().removeClass('show');
          $this.next().slideUp(350);
          $this.children('.checkmark').toggleClass('checked');
          
          $this.next().css('opacity', 0);
          
      } else {
          $('.checkmark').removeClass('checked');
          $this.children('.checkmark').toggleClass('checked');
          $this.parent().parent().find('li .inner').removeClass('show');
          $this.parent().parent().find('li .inner').slideUp(350);
          $this.next().toggleClass('show');
          $this.next().slideToggle(350);
          $this.next().animate({
          opacity: 1,
          }, 100, function() {
            // Animation complete.
          });
          }
  });


})(jQuery);