
;(function ($) {
	// only runs the code if there is an youtube element on the site
	if (!$('header').length) {
		return;
	}
	
	if ($(window).width() < 800) {
		var height = $('.header-wrapper').height();
		$('.wrapper').css('margin-top', height - 70)
	}
	function sticky_relocate() {
		var window_top = $(window).scrollTop();
		var div_top = $('.header').offset().top;
		// var height = $('.header-wrapper').height();

		if (window_top > div_top) {
			$('.header-wrapper').addClass('stick');
			$('header').height($('.header-wrapper').outerHeight());
		} else {
			$('.header-wrapper').removeClass('stick');
			$('header').height(height);
		}
	}

	$(function() {
		$(window).scroll(sticky_relocate);
		sticky_relocate();
	});

})(jQuery);