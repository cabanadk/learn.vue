$(document).on("click",".mobile-btn",function(e) {
	if (!$('.menu').is(':visible')) {
		$('.menu').slideToggle(350).css('display', 'flex');
		$('body').css('overflow', 'hidden');
		$(this).addClass('icon-close');
		$(this).addClass('icon-menu');
		}
	else {
		$('.menu').slideUp(350);
		$('body').css('overflow', 'auto');
		$(this).addClass('icon-menu');
		$(this).removeClass('icon-close');
	}
	e.preventDefault();
});




$(document).on("click",".dropdown-arrow",function(e) {
	if (!$(this).next('.submenu').is(':visible')) {
		$(this).next('.submenu').slideToggle(350);
		$(this).toggleClass('rotate');
	}
	else {
		$(this).next('.submenu').slideUp(350);
		$(this).toggleClass('rotate');
	}
	e.preventDefault();
});


$( window ).resize(function() {
	$('.submenu').slideUp(350);
	$('.mobile-btn').addClass('icon-menu');
	$('.mobile-btn').removeClass('icon-close');
	$('.dropdown-arrow').removeClass('rotate');
	
	if ($(window).width() > 800) {
		$('.menu').css('display', 'flex');
	} else {
		$('.menu').css('display', 'none');
		
	}
});