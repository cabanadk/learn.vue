;(function ($) {
	if (!$('.expand-list').length) {
		return;
	}

	$(document).on("click",".expand-list-btn",function(e) {
  		if (!$(this).parent().find('.expand-list').is(':visible')) {
  			$(this).parent().find('.expand-list').slideToggle(350);
        $(this).find('i').addClass('rotate');
  		}
  		else {
        	$(this).parent().find('.expand-list').slideUp(350);
          $(this).find('i').removeClass('rotate');
  		}
      e.preventDefault();
    });
	

})(jQuery);