
Vue.component('coupon', {
    template: `
        <input placeholder="Enter your code" @blur="onCouponApplied">
    `,
    methods: {
        onCouponApplied(){
            this.$emit('applied');
        }
    }
});

new Vue({
    el: '#app',
    data: {
        couponApplied: false
    },
    methods: {
        onCouponApplied(){
           this.couponApplied = true;        
        }
    }
});


