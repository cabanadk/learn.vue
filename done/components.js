Vue.component('task-list', {
    template: '<div><task v-for="task in  tasks">{{task.task}}</task></div>',

    data(){
        return {
            tasks: [
                {task: "Driv to soccer", completed: false},
                {task: "Fly drone", completed: false},
                {task: "Make Money", completed: false},
                {task: "Go buy a cat", completed: true},
                {task: "Clean Roome", completed: true},
                {task: "Learn Vue", completed: true}
            ]
        }
    }
})

Vue.component('task', {
    template: '<li><slot></slot></li>'
});


var app = new Vue({
    el: '#app'
});